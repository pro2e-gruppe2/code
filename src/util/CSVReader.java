package util;
import java.io.*;
import java.util.*;

public class CSVReader {
	private static final String delimiter = ";";
	private List<String> list = new ArrayList<String>();
	private int columns = 0;
	private int rows;
	private String[] header;
	private String[] data1d;
	private String[][] data2d;


	public int getRows() {
		return rows;
	}

	public int getColumns() {
		return columns;
	}

	public String[] getHeader() {
		return header;
	}

	public String[][] getData2d() {
		return data2d;
	}

	public CSVReader(String csvFileAdress) {
		read(csvFileAdress);
	}
	
	private void read(String csvFile) {
		try {
			File file = new File(csvFile);
			FileReader fr = new FileReader(file);
			BufferedReader br = new BufferedReader(fr);
			String line = " ";
			String[] tempArr;
			while ((line = br.readLine()) != null) {
				tempArr = line.split(delimiter);
				columns++;
				for (String tempStr : tempArr) {
					list.add(tempStr);
				}
			}
			br.close();
		} catch (IOException ioe) {
			ioe.printStackTrace();
		}
		
		rows = list.size()/columns;
		data1d = list.toArray(new String[list.size()]);
		data2d = new String[columns][rows];
		for(int i=0; i<columns ;i++)
			   for(int j=0;j<rows;j++)
			       data2d[i][j] = data1d[i*rows +j];
		header = data2d[0];
	}

public static void main(String[] args) {
	CSVReader prodDataReader = new CSVReader("C:\\Users\\adria\\Desktop\\ProdData.csv");
	System.out.println(prodDataReader.getData2d()[2][2]);
}

public String test(int i, int i2) {
	// TODO Auto-generated method stub
	return data2d [i][i2];
}	
}
