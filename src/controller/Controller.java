package controller;

import java.awt.FileDialog;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JFrame;

import model.Model;
import model.Recording;

public class Controller extends MockController {

	public Controller(Model model) {
		super(model);
	}

	@Override
	public void buttonImportTimeStamps() {
		view.menu.buttonImportData.setVisible(false);
		readCSV();
		setChannelHistograms();
		setStartStopHistograms();
		Thread thread = new Thread() {
			public void run() {				
				setCorrelation();
			}
		};
		thread.start();
	}

	private void setChannelHistograms() {
		view.channelHistograms.hstChannel1.addValues(model.channel1Timediffs);
		view.channelHistograms.hstChannel2.addValues(model.channel2Timediffs);
		view.menu.buttonChannelHistograms.setVisible(true);
	}

	private void setStartStopHistograms() {
		view.startStopHistograms.start1stop2.addValues(model.start1Stop2);
		view.startStopHistograms.start2stop1.addValues(model.start2Stop1);
		view.menu.buttonStartStopHistogram.setVisible(true);
	}

	private void setCorrelation() {
		model.correlate();
		view.correlation.lineGraph.addValues(model.binvector, model.correlation);
		view.menu.buttonCorrelation.setVisible(true);
	}

	@Override
	public void buttonChannelHistograms() {
		view.channelHistograms.setVisible(true);
		view.startStopHistograms.setVisible(false);
		view.correlation.setVisible(false);
	}

	@Override
	public void buttonStartStopHistogram() {
		view.channelHistograms.setVisible(false);
		view.startStopHistograms.setVisible(true);
		view.correlation.setVisible(false);
	}

	@Override
	public void buttonCorrelation() {
		view.channelHistograms.setVisible(false);
		view.startStopHistograms.setVisible(false);
		view.correlation.setVisible(true);
	}

	private void readCSV() {
		FileDialog fd = new FileDialog(new JFrame());
		fd.setVisible(true);
		File[] f = fd.getFiles();
		String adress = null;
		if (f.length > 0) {
			adress = fd.getFiles()[0].getAbsolutePath();
		}
		if (adress != null) {
			File file = new File(adress);
			FileReader fr;
			try {
				fr = new FileReader(file);
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				return;
			}
			List<Double> channel1TimeDiffs = new ArrayList<Double>();
			List<Double> channel2TimeDiffs = new ArrayList<Double>();
			List<Double> start1Stop2 = new ArrayList<Double>();
			List<Double> start2Stop1 = new ArrayList<Double>();
			BufferedReader br = new BufferedReader(fr);
			String line = "";
			String nextLine = "";
			String[] tempArr;
			double time = -1;
			double nextTime = -1;
			int channel = -1;

			double lastCh1Time = -1;
			double lastCh2Time = -1;

			boolean ch1HasStarted = false;
			boolean ch2HasStarted = false;
			boolean simulteanous = false;
			boolean skip = false;

			double startTime = -1;
			double timePassed = -1;
			int binfactor = 10;
			long recordingSize = (long) Integer.MAX_VALUE * (long) binfactor;

			model.channel1 = new Recording(binfactor);
			model.channel2 = new Recording(binfactor);

			try {
				line = br.readLine();
				startTime = Double.parseDouble(line.split(",")[0]);

				while (line != null) {
					nextLine = br.readLine();
					tempArr = line.split(",");
					time = Double.parseDouble(tempArr[0]);
					channel = Integer.parseInt(tempArr[1].strip());
					timePassed = time - startTime;
					if (nextLine != null) {
						nextTime = Double.parseDouble(nextLine.split(",")[0]);
						simulteanous = time == nextTime;
					} else {
						simulteanous = false;
					}

					if (channel == 1) {
						if (lastCh1Time >= 0) {
							double diff = time - lastCh1Time;
							if (diff < 500_000) { // only consider diffrences upto 500'000 * 81ps = 40.5us
								channel1TimeDiffs.add(diff*81);
							}
						}
						if (simulteanous) {
							start1Stop2.add((double) 0);
							start2Stop1.add((double) 0);
							skip = true;
							ch1HasStarted = false;
							ch2HasStarted = false;
						} else if (lastCh2Time >= 0) {
							if (ch2HasStarted) {
								double lag = time - lastCh2Time;
								ch2HasStarted = false;
								if (lag < 500) { // only consider lags upto 500 * 81ps = 40.5ns
									start2Stop1.add(lag*81);
								}
							}
						}
						if (skip) {
							skip = false;
						} else {
							ch1HasStarted = true;
						}
						if (timePassed < recordingSize) {
							model.channel1.set((int) (timePassed / binfactor));
						}

						lastCh1Time = time;

					}

					else if (channel == 2) {
						if (lastCh2Time >= 0) {
							double diff = time - lastCh2Time;
							if (diff < 500_000) { // only consider diffrences upto 500'000 * 81ps = 40.5us
								channel2TimeDiffs.add(diff*81);
							}
						}
						if (simulteanous) {
							start1Stop2.add((double) 0);
							start2Stop1.add((double) 0);
							skip = true;
							ch1HasStarted = false;
							ch2HasStarted = false;
						} else if (lastCh1Time >= 0) {
							if (ch1HasStarted) {
								double lag = time - lastCh1Time;
								ch1HasStarted = false;
								if (lag < 500) { // only consider lags upto 500 * 81ps = 40.5ns
									start1Stop2.add(lag*81);
								}
							}
						}
						if (skip) {
							skip = false;
						} else {
							ch2HasStarted = true;
						}
						lastCh2Time = time;
						if (timePassed < recordingSize) {
							model.channel2.set((int) (timePassed / binfactor));
						}
					}

					line = nextLine;
				}

			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			model.channel1Timediffs = channel1TimeDiffs.stream().mapToDouble(Double::doubleValue).toArray();
			model.channel2Timediffs = channel2TimeDiffs.stream().mapToDouble(Double::doubleValue).toArray();
			model.start1Stop2 = start1Stop2.stream().mapToDouble(Double::doubleValue).toArray();
			model.start2Stop1 = start2Stop1.stream().mapToDouble(Double::doubleValue).toArray();

		}
	}
}