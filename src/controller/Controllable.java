package controller;

import view.View;

public interface Controllable {
	public void setView(View view);

	public void buttonImportTimeStamps();

	public void menuItemSavePlots();

	public void buttonChannelHistograms();
	

	public void buttonStartStopHistogram();

	public void buttonCorrelation();
}
