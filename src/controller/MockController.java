package controller;

import model.Model;
import view.View;

public class MockController implements Controllable {

	protected Model model;
	protected View view;

	public MockController(Model model) {
		this.model = model;
	}

	public void setView(View view) {
		this.view = view;
		model.addObserver(view);
	}

	@Override
	public void buttonImportTimeStamps() {
		System.out.println("menuItemImportTimeStamps");
	}

	@Override
	public void menuItemSavePlots() {
		System.out.println("menuItemSavePlots");		
	}

	@Override
	public void buttonChannelHistograms() {
		System.out.println("buttonChannelHistograms");		
	}

	@Override
	public void buttonStartStopHistogram() {
		System.out.println("buttonStartStopHistogram");		
	}

	@Override
	public void buttonCorrelation() {
		System.out.println("buttonCorrelation");		
	}

}
