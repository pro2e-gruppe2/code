package view;

import java.awt.Color;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;

import javax.swing.JPanel;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;

public class LineGraph extends JPanel {
	private static final long serialVersionUID = 1L;
	private static final Insets INSETS = new Insets(0, 0, 0, 0);
	private XYSeriesCollection dataset;
	private JFreeChart lineChart;
	private ChartPanel chartPanel;

	public LineGraph(String title) {

		super(new GridBagLayout());

		dataset = new XYSeriesCollection();

		lineChart = ChartFactory.createXYLineChart(title, "Time ps", "Correlation", dataset);

		chartPanel = new ChartPanel(lineChart);

		lineChart.getPlot().setBackgroundPaint(Color.WHITE);
		lineChart.removeLegend();

		add(chartPanel, new GridBagConstraints(0, 0, 1, 1, 1, 1, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
				INSETS, 0, 0));
	}

	public void addValues(int[] x, int[] y) {
		dataset.addSeries(createSeriesFromArrays("Correlation", x, y));
	}
	
    private static XYSeries createSeriesFromArrays(String seriesName, int[] xData, int[] yData) {
        if (xData.length != yData.length) {
            throw new IllegalArgumentException("The xData and yData arrays must have the same length.");
        }

        XYSeries series = new XYSeries(seriesName);
        for (int i = 0; i < xData.length; i++) {
            series.add(xData[i], yData[i]);
        }
        return series;
    }
}
