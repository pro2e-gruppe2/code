package view;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;

import javax.swing.JPanel;

public class StartStopHistograms extends JPanel {
	

	private static final long serialVersionUID = 1L;
	private static final Insets INSETS = new Insets(0, 0, 0, 0);
	
	public LineHistogram start1stop2 = new LineHistogram("Start channel: 1, Stop channel: 2");
	public LineHistogram start2stop1 = new LineHistogram("Start channel: 2, Stop channel: 1");
	
	public StartStopHistograms() {
		super(new GridBagLayout());
		add(start1stop2, new GridBagConstraints(0, 0, 1, 1, 1, 1, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
				INSETS, 0, 0));
		add(start2stop1, new GridBagConstraints(0, 1, 1, 1, 1, 1, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
				INSETS, 0, 0));
	}

}