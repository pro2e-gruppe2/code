package view;

import java.awt.Color;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;

import javax.swing.JPanel;

import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.xy.XYLineAndShapeRenderer;
import org.jfree.data.xy.DefaultXYDataset;


public class SingleSensorGraph extends JPanel
{
	private static final long serialVersionUID = 1L;
	
	DefaultXYDataset dataset = new DefaultXYDataset();
	XYPlot plot = new XYPlot(dataset, new NumberAxis("x"), new NumberAxis("y"),
			new XYLineAndShapeRenderer(false, false));
	
	JFreeChart chart;
	
	ChartPanel chartPanel;
	
	XYLineAndShapeRenderer renderer = (XYLineAndShapeRenderer) plot.getRenderer();
	
	
	public SingleSensorGraph()
	{
		super(new GridBagLayout());

		chart = new JFreeChart("VCO Data", JFreeChart.DEFAULT_TITLE_FONT, plot, true);

		chart.getPlot().setBackgroundPaint(Color.WHITE); // Hintergrundfarbe auf Weiß setzen
		chart.removeLegend();

		chartPanel = new ChartPanel(chart); // Aus dem chart wird ein chartPanel instanziert

		add(chartPanel, new GridBagConstraints(0, 0, 1, 1, 1, 1, GridBagConstraints.NORTH,
				GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
	}
}
