package view;

import java.awt.Color;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.util.Arrays;

import javax.swing.JPanel;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;

public class LineHistogram extends JPanel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private static final Insets INSETS = new Insets(0, 0, 0, 0);
	private XYSeriesCollection dataset;
	private JFreeChart histogram;
	private ChartPanel chartPanel;
	private int NumBins = 250;

	public LineHistogram(String title) {
		super(new GridBagLayout());
		
		dataset = new XYSeriesCollection();

		histogram = ChartFactory.createXYLineChart(title, "Time 81ps", "Counts", dataset);

		chartPanel = new ChartPanel(histogram);

		histogram.getPlot().setBackgroundPaint(Color.WHITE);
		histogram.removeLegend();
		
		add(chartPanel, new GridBagConstraints(0, 0, 1, 1, 1, 1, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
				INSETS, 0, 0));
	}

	public void addValues(double[] values) {
		dataset.addSeries(calculateHistogram(values, NumBins));
	}

	private static XYSeries calculateHistogram(double[] data, int numBins) {
		// Find min and max of the data
		double min = Arrays.stream(data).min().orElse(0);
		double max = Arrays.stream(data).max().orElse(0);

		// Calculate bin width
		double binWidth = (max - min) / numBins;

		// Initialize bin frequencies
		int[] frequencies = new int[numBins];
		double[] binEdges = new double[numBins + 1];

		// Set bin edges
		for (int i = 0; i <= numBins; i++) {
			binEdges[i] = min + i * binWidth;
		}

		// Count frequencies
		for (double value : data) {
			int binIndex = (int) ((value - min) / binWidth);
			if (binIndex >= numBins) {
				binIndex = numBins - 1; // Handle edge case for max value
			}
			frequencies[binIndex]++;
		}

		// Create series for the histogram
		XYSeries series = new XYSeries("Histogram Data");
		for (int i = 0; i < numBins; i++) {
			double binCenter = (binEdges[i] + binEdges[i + 1]) / 2.0;
			series.add(binCenter, frequencies[i]);
		}

		return series;
	}
}
