package view;

import java.awt.Color;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;

import javax.swing.JPanel;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.data.statistics.HistogramDataset;

public class Histogram extends JPanel {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private static final Insets INSETS = new Insets(0, 0, 0, 0);
	
	private int NumBins = 2000;

	private HistogramDataset dataset;
	private JFreeChart histogram;
	private ChartPanel chartPanel;

	public Histogram(String title) {
		super(new GridBagLayout());

		dataset = new HistogramDataset();

		histogram = ChartFactory.createHistogram(title, "Time ps", "Counts", dataset);

		chartPanel = new ChartPanel(histogram);

		histogram.getPlot().setBackgroundPaint(Color.WHITE);
		histogram.removeLegend();

		add(chartPanel, new GridBagConstraints(0, 0, 1, 1, 1, 1, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
				INSETS, 0, 0));
	}

	public void addValues(double[] values) {
		dataset.addSeries("Timestamps", values, NumBins);
	}
}
