package view;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Time;

import javax.swing.JPanel;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.event.MenuEvent;
import javax.swing.event.MenuListener;

import controller.Controllable;
import model.Model;
import util.Observable;
import util.Observer;

public class View extends JPanel implements Observer, ActionListener, ChangeListener, MenuListener
{
	private static final long serialVersionUID = 1L;

	private static final Insets INSETS = new Insets(0, 0, 0, 0);

	private Controllable controller;
	
	public Menu menu;
	
	public ChannelHistograms channelHistograms;
	
	public StartStopHistograms startStopHistograms;
	
	public Correlation correlation;

	public void init()
	{
		menu = new Menu(controller);
		add(menu, new GridBagConstraints(0, 0, 1, 1, 0, 0, GridBagConstraints.NORTHWEST, GridBagConstraints.NONE,
				INSETS, 0, 0));
		
		channelHistograms =  new ChannelHistograms();
		add(channelHistograms, new GridBagConstraints(0, 1, 1, 1, 1, 1, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
				INSETS, 0, 0));
		
		
		startStopHistograms =  new StartStopHistograms();
		add(startStopHistograms, new GridBagConstraints(0, 1, 1, 1, 1, 1, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
				INSETS, 0, 0));
		startStopHistograms.setVisible(false);
		
		correlation = new Correlation();
		add(correlation, new GridBagConstraints(0, 1, 1, 1, 1, 1, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
				INSETS, 0, 0));
		correlation.setVisible(false);
		
	}

	public View(Controllable controller)
	{
		super(new GridBagLayout());
		this.controller = controller;

	}

	@Override

	public void update(Observable obs, Object obj)
	{
		Model model = (Model) obs;
	}

	@Override
	public void actionPerformed(ActionEvent e)
	{

	}

	@Override
	public void stateChanged(ChangeEvent e)
	{

	}

	@Override
	public void menuSelected(MenuEvent e)
	{
		
	}

	public void menuDeselected(MenuEvent e)
	{

	}

	@Override
	public void menuCanceled(MenuEvent e)
	{

	}
}
