package view;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;

import javax.swing.JPanel;

public class Correlation extends JPanel {
	

	private static final long serialVersionUID = 1L;
	private static final Insets INSETS = new Insets(0, 0, 0, 0);
	
	public LineGraph lineGraph = new LineGraph("Correlation");
	
	public Correlation() {
		super(new GridBagLayout());
		add(lineGraph, new GridBagConstraints(0, 0, 1, 1, 1, 1, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
				INSETS, 0, 0));
	}

}
