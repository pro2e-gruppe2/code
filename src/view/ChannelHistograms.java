package view;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;

import javax.swing.JPanel;

public class ChannelHistograms extends JPanel {
	

	private static final long serialVersionUID = 1L;
	private static final Insets INSETS = new Insets(0, 0, 0, 0);
	
	public Histogram hstChannel1 = new Histogram("Channel 1 Histogram");
	public Histogram hstChannel2 = new Histogram("Channel 2 Histogram");
	
	public ChannelHistograms() {
		super(new GridBagLayout());
		add(hstChannel1, new GridBagConstraints(0, 0, 1, 1, 1, 1, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
				INSETS, 0, 0));
		add(hstChannel2, new GridBagConstraints(0, 1, 1, 1, 1, 1, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
				INSETS, 0, 0));
	}

}
