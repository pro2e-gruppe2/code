package view;

import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;

import controller.Controllable;

public class Menu extends JPanel implements ActionListener {
	private Controllable controller;

	private JMenuBar menuBar = new JMenuBar();
	public JButton buttonImportData = new JButton("Import data");
	public JButton buttonChannelHistograms = new JButton("Channel histograms");
	public JButton buttonStartStopHistogram = new JButton("Start-Stop histograms");
	public JButton buttonCorrelation = new JButton("Correlation");

	public Menu(Controllable controller) {
		this.controller = controller;
		//setLayout(new GridBagLayout());
		add(menuBar);
		menuBar.add(buttonImportData);
		
		buttonImportData.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				controller.buttonImportTimeStamps();
			}
		});
		
		menuBar.add(buttonChannelHistograms);
		buttonChannelHistograms.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				controller.buttonChannelHistograms();
			}
		});
		buttonChannelHistograms.setVisible(false);
		
		
		menuBar.add(buttonStartStopHistogram);
		buttonStartStopHistogram.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				controller.buttonStartStopHistogram();
			}
		});
		buttonStartStopHistogram.setVisible(false);
		
		
		menuBar.add(buttonCorrelation);
		buttonCorrelation.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				controller.buttonCorrelation();
			}
		});
		buttonCorrelation.setVisible(false);
	}


	@Override
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub
		
	}

}
