package model;

import java.util.BitSet;

public class Recording {
	int binfactor;
	BitSet data;
	
	public Recording(int binfactor) {
		this.binfactor = binfactor;
		data = new BitSet();
	}
	
	public void set(int index) {
		data.set(index);
	}
	
	public boolean get(int index) {
		return data.get(index);
	}
}
