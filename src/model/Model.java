package model;

import java.util.BitSet;

import util.Observable;

public class Model extends Observable {
	public double[] channel1Timediffs;
	public double[] channel2Timediffs;
	public double[] start1Stop2;
	public double[] start2Stop1;
	public Recording channel1;
	public Recording channel2;
    public int[] binvector; 		//holds the current offset(lag) as a factor of 81ps.
    public int[] correlation;	//holds the correlation result for the current offset
	public void notifyObservers() {
		setChanged();
		notifyObservers(this);
	}
	public void correlate() {
		int binfactor = channel1.binfactor == channel2.binfactor ? channel1.binfactor : 0;
		int limit = 150;								//150 * 81ps = 12.15 ns
        int max_offset = Math.floorDiv(limit, binfactor); //calculates how many offsets are within 12.15 ns, given a.b have been recorded with the binfactor.
        int min_offset = -max_offset;
        int offset = min_offset;
        int recordingSize = channel1.data.length() >= channel2.data.length() ? channel1.data.length() : channel2.data.length();
        correlation = new int[2*max_offset+1];
        binvector = new int[2*max_offset+1];
        int pos = 0;
        int bin;
        
        while (offset <= max_offset){
        	
            bin = offset*binfactor;
            binvector[pos] = bin*81;

            int sum = 0;
            int idx1 = 0;
            int idx2;
            
  
            while (idx1<recordingSize){
                idx2 = idx1+offset;
                if (idx2<0) {
                    idx2 = 0;
                    idx1 = idx2-offset;
                	continue;
                }
                if (idx2 > (recordingSize-1)) {                	
                	break;
                }
                if(channel1.data.get(idx1) && channel2.data.get(idx2)) {
                	sum++;
                }
                idx1++;
                
            }
            correlation[pos] = sum;
            offset++;
            pos++;
        }
	}
	public static void main(String[] args) {
		Model model =  new Model();
		model.channel1 = new Recording(10);
		model.channel2 = new Recording(10);
		model.channel1.set(5);
		model.channel2.set(10);
        model.channel1.set(20);
        model.channel2.set(25);
        model.correlate();
        System.out.println(model.correlation[20]);
		
	}
}