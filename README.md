# FPY Analysesoftware - Projekt 2, Team 2
Autoren: Dominik Bernhard, Adriano Ritzman, Tobias Kissling,
Devesh Sharat, Sebastian Werka, Leander Graf
## Einsatz der Software
Die Software wird verwendet um Distanzmessensoren zu prüfen. Als Input benötigt die Software hierbei 2x Files vom Typen CSV. 1x ProductionData.csv und 1x RxTx.csv.
Innerhalb der Software stehen dem Benutzer Schieberegler zur Verfügung um die Grenzwerte anzupassen. Anhand der vom Benutzer eingestellten Grenzwerte berechnet die Software den FirstPassYield (FPY). Grafisch unterstützt wird der Benutzer von diversen Diagrammen, welche die Daten der Sensoren darstellen. 

## Installieren der Software
- Klone das Git Repository
```
git clone git@gitlab.fhnw.ch:pro2e-gruppe2/code.git
```

-


* Öffne eine Java Programmierumgebung (bsp Eclipse IDE for Java Developers)
+ Thomas Jefferson



git markup 

## Bedienung der Software
- Navigiere im src Fileordner zum Package "app".
- Öffne Klasse "App.java"
- Führe das Programm mit "Run" aus. 

Hat bisher alles geklappt, so öffnet sich die Applikation
![Erstansicht_ohneGraphs](/uploads/b7ed5ed2d2316a2d6c4eefbb1b51a1d3/Erstansicht_ohneGraphs.png)

- Importiere die Daten über den Reiter "Import/Save Data"
- Importiert werden muss "ProductionData.csv" und "RxTx-Data.csv"

Hat dies geklappt, ist folgendes Bild zu stehen![Erstansicht_DatenImportiert](/uploads/732415f1e01697c7956157c662883d0c/Erstansicht_DatenImportiert.png)
