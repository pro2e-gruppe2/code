# FPY Analysesoftware - Projekt 2, Team 2
Autoren: Dominik Bernhard, Adriano Ritzman, Tobias Kissling,
Devesh Sharat, Sebastian Werka, Leander Graf
## Einsatz der Software
Die Software wird verwendet um Distanzmessensoren zu prüfen. Als Input benötigt die Software hierbei 2x Files vom Typen CSV. 1x ProductionData.csv und 1x RxTx.csv.
Innerhalb der Software stehen dem Benutzer Schieberegler zur Verfügung um die Grenzwerte anzupassen. Anhand der vom Benutzer eingestellten Grenzwerte berechnet die Software den FirstPassYield (FPY). Grafisch unterstützt wird der Benutzer von diversen Diagrammen, welche die Daten der Sensoren darstellen. 

## Installieren der Software
- Klone das Git Repository
```
git clone git@gitlab.fhnw.ch:pro2e-gruppe2/code.git
```

- Öffne eine Java Programmierumgebung (bsp Eclipse IDE for Java Developers)
- Importiere das Repository in deine Programmierumgebung

## Bedienung der Software
- Navigiere zum "src" Filefolder
- Klappe Package "app"aus
- Öffne Klasse "App.java"
- Führe das Programm mit "Run" aus 

Hat bisher alles geklappt, so öffnet sich die Applikation
![Erstansicht_ohneGraphs](/uploads/b7ed5ed2d2316a2d6c4eefbb1b51a1d3/Erstansicht_ohneGraphs.png)

- Importiere die Daten über die Menübar unter "Import/Save Data"
- Importiert werden muss "ProductionData.csv" und "RxTx-Data.csv"


Hat dies geklappt, ist folgendes Bild zu sehen![Erstansicht_DatenImportiert](/uploads/732415f1e01697c7956157c662883d0c/Erstansicht_DatenImportiert.png)

- Um konkrete Grafen zu betrachten, gehe in der Menübar auf "Select Shown Graphs"
- Um nur eine bestimmte Anzahl an Sensoren zu betrachten, gehe in der Menübar auf "Select Sensors"

Folgendes Bild zeigt "Select Shown Graphs" und "Select Sensors" in Anwendung
![AuswahlGrafen_und_Sensorenbereich](/uploads/f7afda021f6633c90d4b7b2af3f391ee/AuswahlGrafen_und_Sensorenbereich.png)
Anstelle der insgesamt 1'000 Sensoren, sind beispielsweise lediglich 131 Sensoren grafisch dargestellt und dies mit den vier vom Benutzer eingestellten Grafen.

- Um die Ausgabe der Datenfelder (Textfelder oberhalb der Grafen) anzupassen, gehe in der Menübar auf "Data Fields
- Wähle hier den gewünschten Parameter aus
- Ausgegeben wird der "Average", "Minimum Peak" und "Maximum Peak"

Folgendes Bild zeigt die Verwendung der "Data Fields" am Beispiel der "Sum of Deviation" Ausgabe
![Datenfelder_SumOfDeviation](/uploads/d07cab6eb778ad14553391b09a34caa5/Datenfelder_SumOfDeviation.png)

- Um die Parameter für den FirstPassYield (FPY) einzustellen, gehe in der Menübar auf "Limits FPY"
- Die Parameter können hier wahlweise per Slider oder über Tastatureingabe eingestellt werden

Unter der Verwendung der "Limits FPY" Parameter fallen gewisse Sensoren durch. Folgendes Bild zeigt eine sehr strenge Bewertung der "Limits FPY" Parameter. Die strenge Bewertung führt dazu, dass hier nur noch 8% der Sensoren die Anforderungen an die Parameter bestehen. 
![AnwendungSlider_LimitsFPY](/uploads/68ce325e0a5644650654a12a4b6484cf/AnwendungSlider_LimitsFPY.png)

- Um einen konkreten Sensor zu betrachten kann in der Menübar "Compare Sensors" angewählt werden. 

Folgendes Bild zeigt die Ansicht für einen konkreten Sensor. Der Sensor wird vom Benutzer unter "Choose sensor:" ausgewählt. 
![Ansicht_CompareSensors](/uploads/ec2ffec9146e980fc849a7e79cd7dd89/Ansicht_CompareSensors.png)
Auf dem Bild ist direkt ersichtlich, ob der Sensor die Kriterien der "Limits FPY" erfüllt. In diesem Beispiel fällt der Sensor beim R&#178;-Value durch, was wiederum grafisch in Rot dargestellt wird. 

## Speichern der Testergebnisse
- Um die Testergebnisse zu speichern gehe in der Menübar zu "Import/Save Data"
- Wähle hier im Reiter "Save Analysed Data" aus

Erstellt wird ein CSV-File. Das generierte File enthält nicht nur alle Daten der Sensoren, sondern auch die Einstellungen des "Limits FPY". 
![Output_Data](/uploads/add5a63d6e29f145817718cf5e9d0275/Output_Data.png) 
Durch diese Daten ist es nachvollziehbar, unter welchen Parametern die Sensoren getestet wurden. 



